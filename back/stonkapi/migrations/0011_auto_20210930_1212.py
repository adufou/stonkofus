# Generated by Django 3.1.12 on 2021-09-30 10:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stonkapi', '0010_auto_20210930_0152'),
    ]

    operations = [
        migrations.RenameField(
            model_name='item',
            old_name='itemBankId',
            new_name='itemBank',
        ),
        migrations.RenameField(
            model_name='itemprice',
            old_name='itemId',
            new_name='item',
        ),
        migrations.RenameField(
            model_name='report',
            old_name='itemPriceId',
            new_name='itemPrice',
        ),
        migrations.RenameField(
            model_name='report',
            old_name='userId',
            new_name='user',
        ),
        migrations.RenameField(
            model_name='trade',
            old_name='buyTransactionId',
            new_name='buyTransaction',
        ),
        migrations.RenameField(
            model_name='trade',
            old_name='sellTransactionId',
            new_name='sellTransaction',
        ),
        migrations.RenameField(
            model_name='trade',
            old_name='userId',
            new_name='user',
        ),
        migrations.RenameField(
            model_name='transaction',
            old_name='itemPriceId',
            new_name='itemPrice',
        ),
        migrations.RenameField(
            model_name='transaction',
            old_name='userId',
            new_name='user',
        ),
    ]
